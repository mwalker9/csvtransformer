from typing import Dict, Any
import yaml

class TransformationConfig:

    def __init__(self, yaml_file_path):
        with open(yaml_file_path, "r") as stream:
            config = yaml.safe_load(stream)
        transformation_job = config["transformation_job"]
        self.source_config = transformation_job["source"]
        self.target_config = transformation_job["target"]
    
    def get_source_config(self) -> Dict[str, Any]:
        return self.source_config

    def get_target_config(self) -> Dict[str, Any]:
        return self.target_config

