import re
from typing import Dict,List

class DataRow:

    def __init__(self, line: List[str], field_config: List[Dict], line_number: int):
        self.line_number = line_number
        field_names = [field["name"] for field in field_config]
        self.is_source_error = len(line) != len(field_config)
        self.original_line = line
        if self.is_source_error:
            self.set_error("The source config had different number of fields than line.")
            return
        self.source_data_map = {field_names[i]: line[i] for i in range(len(line))}
        self.source_validation = {field_config[i]["name"]: field_config[i].get("validation_regex", None) for i in range(len(field_config))}

    def validate_source(self) -> None:
        for field_name, validation_regex in self.source_validation.items():
            if validation_regex is None:
                continue
            raw_data = self.source_data_map[field_name]
            if not re.match(validation_regex, raw_data):
                self.set_error("{} did not match regex of {}".format(field_name, validation_regex))
                return


    def set_error(self, error_message: str) -> None:
        self.is_source_error = True
        self.error_message = error_message

    def safe_get(self, dict: Dict, field_name: str) -> str:
        if field_name in dict:
            return dict[field_name]
        else:
            self.set_error("could not find field {}".format(field_name))
            return ""


    def get_concat(self, field_names: List[str], concat_char: str) -> str:
        if self.is_source_error:
            return ""
        else:
            return concat_char.join([self.safe_get(self.source_data_map, field_name) for field_name in field_names])


    def get_raw(self, field_name: str) -> str:
        if self.is_source_error:
            return ""
        return self.safe_get(self.source_data_map, field_name)

    def get_error_message(self):
        return "Error on line {}: {}".format(self.line_number, self.error_message)