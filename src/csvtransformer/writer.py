import csv
import dateutil.parser
from decimal import Decimal
from typing import Dict, Any, IO

from data_row import DataRow

class Writer:

    def __init__(self, transformation_config: Dict[str, Any], target_file_path: str):
        self.config = transformation_config
        self.target_file_path = target_file_path

    def convert_value_to_type(self, value: str, target_type: str) -> Any:
        if target_type.lower() == "string":
            return value
        elif target_type.lower() == "integer":
            return int(Decimal(value.replace(",", "")))
        elif target_type.lower() == "bigdecimal":
            return Decimal(value.replace(",", ""))
        elif target_type.lower() == "datetime":
            return dateutil.parser.parse(value)
        else:
            raise ValueError


    def translate_source(self, row: DataRow, source: Dict[str, Any]) -> str: 
        try:
            if "raw" in source:
                return row.get_raw(source["raw"])
            elif "constant" in source:
                return source["constant"]
            elif "concat" in source:
                concat_config = source["concat"]
                return row.get_concat(concat_config["fields"], concat_config["char"])
            else:
                raise ValueError
        except ValueError:
            print("Unexpected config error - didn't understand source specification in target.")
            raise

    def start_file_write(self) -> IO[Any]:
        field_names = [field["name"] for field in self.config["fields"]]
        with open(self.target_file_path, mode='w') as file_descriptor:
            writer = csv.writer(file_descriptor)
            writer.writerow(field_names)
        return open(self.target_file_path, mode='a')    

    def write_row(self, row: DataRow, file_descriptor: IO[Any]) -> None:
        if row.is_source_error:
            print(row.get_error_message())
            return
        
        sources = [field["source"] for field in self.config["fields"]]

        raw_data = [self.translate_source(row, source) for source in sources]

        types = [field["type"] for field in self.config["fields"]]

        converted_data = map(self.convert_value_to_type, raw_data, types)

        if row.is_source_error:
            print(row.get_error_message())
        else:
            writer = csv.writer(file_descriptor)
            writer.writerow(converted_data)


        
        

        
