import argparse

from reader import Reader
from transformation_config import TransformationConfig
from writer import Writer

def run_transform(source_file: str, config_file: str, target_file: str):
    config = TransformationConfig(args.config_file)

    reader = Reader(config.get_source_config(), args.source_file)

    writer = Writer(config.get_target_config(), args.target_file)

    with writer.start_file_write() as f:
        for row in reader.get_rows():
            row.validate_source()
            writer.write_row(row, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Transforms CSV according to config and writes to target file.')
    parser.add_argument('source_file', type=str, help="The file to transform")
    parser.add_argument('config_file', type=str, help="The yaml file to use as the transformation config")
    parser.add_argument('target_file', type=str, help="The file path to write out to")
    args = parser.parse_args()
    run_transform(args.source_file, args.config_file, args.target_file)

   




