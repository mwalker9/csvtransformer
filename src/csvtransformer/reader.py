import csv
from typing import Dict, Generator

from data_row import DataRow

class Reader:

    def __init__(self, source_config: Dict, file_name):
        self.file_name = file_name
        self.fields = source_config["fields"]
        self.has_header = source_config["has_header"]

    def get_rows(self) -> Generator[DataRow, None, None]:
        with open(self.file_name) as f:
            reader = csv.reader(f)
            starting_line = 1
            if self.has_header:
                header = next(reader)
                starting_line += 1
                print("--FYI ONLY-- SOURCE HEADER OF: {}".format(",".join(header)))
            for row_number, row in enumerate(reader, start=starting_line):
                yield DataRow(row, self.fields, row_number)
