from reader import Reader

def test_get_rows():
    reader = Reader(
        {
            "fields": [
                {"name": "pie"}
            ],
            "has_header": True
        },
        "./test1.csv"
    )
    line_generator = reader.get_rows()
    first_line_data = next(line_generator)
    assert first_line_data.original_line == ['5', '2020', '12', '6', 's', '7', '8', '', 'pizza']