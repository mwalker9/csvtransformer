from data_row import DataRow
from typing import List

def create_data_row(data: List[str]) -> DataRow:
    return DataRow(
        data,
        [
            {"name": "bob", "validation_regex": "\\w"},
            {"name": "number", "validation_regex": "\\d{3}"},
            {"name": "stuff", "validation_regex": "(\\d\\.?)+"},
            {"name": "letters", "validation_regex": "[A-Z]+"}
        ],
        5
    )

def test_validate_source():
    data_row = create_data_row(["a", "125", "6.0.999", "lLj"])
    data_row.validate_source()
    assert data_row.is_source_error
    assert data_row.error_message.startswith("letters ")
    data_row = create_data_row(["a", "125", "6.0.999", "LLJ"])
    data_row.validate_source()
    assert not data_row.is_source_error
    


def test_safe_get():
    data_row = create_data_row(["a", "a", "a", "a"])
    good_data = data_row.safe_get({"pizza": "pie"}, "pizza")
    assert good_data == "pie"

    assert not data_row.is_source_error
    bad_data = data_row.safe_get({"pizzeria": "pie"}, "pizza")
    assert bad_data == ""
    assert data_row.is_source_error

def test_get_concat():
    data_row = create_data_row(["a", "b", "c", "d"])
    assert data_row.get_concat(["bob", "bob", "number", "letters"], "?") == "a?a?b?d"

def test_get_raw():
    data_row = create_data_row(["a", "b", "c", "d"])
    assert data_row.get_raw("stuff") == "c"
