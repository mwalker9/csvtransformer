import pytest
from typing import Dict, List

from data_row import DataRow
from writer import Writer

def create_data_row(data: List[str]) -> DataRow:
    return DataRow(
        data,
        [
            {"name": "bob"},
            {"name": "number"},
            {"name": "stuff"},
            {"name": "letters"}
        ],
        5
    )

def get_writer(source: Dict[str, str], field_type: str) -> Writer:
    return Writer(
        {
            "fields": [
                {
                    "name": "OrderId",
                    **source,
                    "type": field_type
                }
            ]
        },
        "target_file"
    )

def test_convert_value_to_type():
    writer = get_writer({}, "integer")
    assert writer.convert_value_to_type("6,000", "INTEGER") == 6000
    assert writer.convert_value_to_type("6,000.54", "INTEGER") == 6000
    assert writer.convert_value_to_type("6,000.54", "string") == "6,000.54"
    assert writer.convert_value_to_type("2021-01-14", "datetime").month == 1

    with pytest.raises(ValueError):
        writer.convert_value_to_type("5", "not-a-known-type")



def test_translate_source():
    row = create_data_row(["1", "2", "pizza", "4"])
    writer = get_writer({}, "integer")
    assert writer.translate_source(row, {"raw": "bob"}) == "1"
    assert writer.translate_source(row, {"concat": {"char": "-", "fields": ["stuff", "stuff"]}}) == "pizza-pizza"
    assert writer.translate_source(row, {"constant": "bob"}) == "bob"