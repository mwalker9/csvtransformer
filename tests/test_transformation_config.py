from transformation_config import TransformationConfig

def test_transformation_config():
    config = TransformationConfig("./sample_config.yml")
    assert config.get_source_config()["fields"][0]["name"] == "Order Number"
    assert config.get_target_config()["fields"][0]["type"] == "Integer"