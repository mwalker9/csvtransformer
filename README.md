# CSVTransformer

Simple configurable program that reads in a CSV, validates the format of the fields, does a configurable transformation, and writes out

## How to Build and Run

This code can either be executed as a stand-alone python program or imported as a library and then executed programmatically.  The only requirements for this program are an environment running python3.7+ with pyyaml and python-dateutil packages installed.

### Running as stand-alone program

This program can be executed by calling the [`main.py`](https://gitlab.com/mwalker9/csvtransformer/-/blob/master/src/csvtransformer/main.py) file and passing as positional arguments: the source file path, the configuration file path, and the target file path.

### Running as library

Alternatively, it should be possible to import the code from a different python project and invoke the [run_transform](https://gitlab.com/mwalker9/csvtransformer/-/blob/master/src/csvtransformer/main.py#L7) method directly, passing in as parameters the source file, config file, and target file paths respectively.

## Architectural Overview

The architecture of the project is inspired by the typical ETL framework.  There is a `Reader` class that extracts the data.  The data is encapsuled by a `DataRow` class since data is processed one row at a time. Then, there is a a `Writer` class that processes the transformations and writes out the data row by row.  Presently, the writer houses most of the transformation logic, which could be improved on in the future by extracting the transformation layer out into its own class(es) which may also improve the ease of adding additional transformation types in the future.

### Transformation Configuration

The transformation is configured with a yaml file.  The [sample_config.yml](https://gitlab.com/mwalker9/csvtransformer/-/blob/master/sample_config.yml) file is a sample of the required schema.

#### Transformation Configuration Schema

The required fields of the configuration is as follows:

```
transformation_job:
    source:
        fields: <An ARRAY of SOURCE FIELDS>
        has_header: <BOOLEAN indicating if the source CSV has a header row>
    target:
        fields: <An ARRAY of TARGET FIELDS>
```

**Source Fields** are objects that have a `name` attribute and an OPTIONAL `validation_regex` attribute.  The source fields must be listed in the same order as the fields appear in the CSV, as the configuration file is treated as the single source of truth for field naming and ordering.  The `validation_regex` attribute is used to validate that data matches given regular expressions.  If the regular expression for any field in a row is not met, the row is not written out to the file.  Instead, an error is logged.

**Target Fields** are objects that describe a field that should be written out to the target file.  A target field has a `name` attribute, a `source` attribute, and a `type` attribute.  
* The `name` attribute is used to write the header row at the top of the target CSV.
* The `source` attribute describes where the data for this target field comes from.  There are currently three supported types of "sources": `concat`, `raw`, and `constant`.
    * The `concat` source requires a `char` string value and a list of source file field names.  This tells the program to concatenate the fields together with the given `char` in between to produce the final target field. 
    * The `raw` source tells the program to just use the source field value with the given name.
    * The `constant` source tells the program to use the given `constant` as the value of the field.

Source examples:
```yaml
#concat
source: 
    concat: 
        char: "-"
        fields: 
            - Year
            - Month
            - Day

#raw
source:
    raw: "source_field_name"

#constant
source:
    constant: "some constant that will be repeated for every line"


```

* The `type` attribute is required and allows for some data manipulation.  The data is cast to the given type before being written out to the target file (but after any given `concat` operations).  This allows for some data transformations such as truncating decimals.  The supported types are currently `String`, `BigDecimal`, `DateTime`, and `Integer`.

## Assumptions

This setup assumes that only CSVs will be used.  This setup also assumes that the default Python CSV parsing configuration will be sufficient to read and write the CSVs.

## Possible Next Steps


* Make the transformation layer more explicit by pulling transformations out into their own Python classes.  Also move the transformation configuration into its own configuration section in the yaml file.
* More end-to-end testing.
* Do more formal configuration validation against a schema.
* The requirements for running the tests/code would be better managed in a typical `requirements.txt` file.
* Using this code as a library would be easier if it was uploaded to pypi.